library(caret)
set.seed(3214)
##Feature Selection workflow
###1.Split data into training and test sets with createDataPartition

split_into_training_and_test<-function(df,number_of_partitions=1,outcome,percent_to_training=0.75){
  #df is the dataset as a dataframe
  #Number of partitions is the number of random sampled partitions to make
  #outcome is the column containing the outcome
  #percent_to_training is the percent of the data to us in training
  
  #Returns a vector of the row indices that have been put into the training set
  #Can split the dataframe into training and test sets with code like this:
  ##########trainDescr=df[inTrain,]
  ##########testDescr=df[-inTrain,]
  #Instead of taking in a column name splitting using an actual vector may be better with a vector for the
  #downstream analysis
  
  
  inTrain=createDataPartition(y=df[[outcome]],
                               times=number_of_partitions,
                               p=percent_to_training,
                              list=FALSE)
  
  
  return (inTrain)
  
}
#######(May not be appropriate if dataset is small like the Corey dataset) 
#######For Corey dataset Charlie recommends just peforming cross-validation
###2.Remove features with little variance with nearZeroVar

find_low_variance_features<-function(df,freqCut = 95/5, uniqueCut = 10){
  #Return column names of near zero variance features
  #See nearZeroVar documentation of descriptions of freqCut and uniqueCut
  problamatic_cols=nearZeroVar(x=df,
                               freqCut=freqCut,
                               uniqueCut=uniqueCut,
                               names=TRUE)
  return(problamatic_cols)
  
}
###3.Remove highly correlated features with findCorrelation
get_numeric_cols_from_dataframe<-function(df){
  #Returns a matrix of data columns from a data frame if the data type is numeric
  v_is_numeric_bool=sapply(df,is.numeric)
  return_matrix=as.matrix(df[,v_is_numeric_bool])
  return(return_matrix)
}
find_highly_correlated_columns<-function(df,cutoff=0.9,correlation_method="pearson",use="pairwise.complete.obs"){
  #Returns a vector of column names that should be removed because they have a high correlation with at
  #least one other column. See documentation for findCorrelation for more details
  #See documentation for cor for a description of what use means
  numeric_values_matrix=get_numeric_cols_from_dataframe(df)
  correlations=cor(x=numeric_values_matrix,
                   method=correlation_method,
                   use=use)
  View(correlations)
  problamatic_cols=findCorrelation(x=correlations,
                                   cutoff=cutoff,
                                   names=TRUE)
     
   return (problamatic_cols)
}

###4.Normalize featues using preProcess
######Charlie recommends 
###5.Tune model parameters with train
######availble re-sampling methods in train: bootstrapping, k-fold cross-validation, leave-one-out cross validation,
######and leave-group-out cross-validation
######train only accepts numberic input
######In general caret assumes no missing data or that missing have already been replaced with other values

#In data
#infile="Sample_Data/prison_data_layout_b.csv"
infile="Sample_Data/sample_chem_data.csv"

#Command options
outcome="agency"
#outcome="outcome"
number_of_partitions=1
percent_to_training=0.75
df_in=read.csv(infile)
# paritions=split_into_training_and_test(df=df_in,
#                              number_of_partitions=number_of_partitions,
#                              outcome=outcome,
#                              percent_to_training=percent_to_training)
# low_variance_cols=find_low_variance_features(df_in)
#View(get_numeric_cols_from_data_frame(df_in))
print(find_highly_correlated_columns(df_in))



#rm(list=ls())